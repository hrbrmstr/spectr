#' Read HTML from Chrome remote interface javascript-interpreted HTML
#'
#' @param url URL to load into Chromium
#' @param as text or parsed
#' @param cri_host,cri_port host and port Chrome remote interface is running on.
#' @param user_agent User-Agent string
#' @export
cri_read_html <- function(url, as = c("text", "parsed"),
                          cri_host = "localhost", cri_port = 9222L,
                          user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12") {

  as <- match.arg(as[1], c("text", "parsed"))

  processx::run(
    command = Sys.which("node"),
    args = c(
      system.file("js/dump-html.js", package = "spectr"),
      url,
      cri_host,
      cri_port,
      user_agent
    ),
    echo_cmd = getOption("SPECTR_DEBUG", FALSE),
    echo = getOption("SPECTR_DEBUG", FALSE)
  ) -> res

  switch(
    as,
    text = res$stdout,
    parsed = xml2::read_html(res$stdout)
  )

}