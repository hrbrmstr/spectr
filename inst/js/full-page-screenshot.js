const CDP = require('chrome-remote-interface');

const target = process.argv[2] + "";

const options = {
  host: process.argv[3] + "",
  port: process.argv[4] * 1
};

const user_agent = process.argv[5] + "";

const format = process.argv[6] + "";
const viewportWidth = process.argv[7] * 1;

// Start the Chrome Debugging Protocol
CDP(options, async(client) => {

  // Extract used DevTools domains.
  const { DOM, Emulation, Network, Page } = client;

  // Enable events on domains we are interested in.
  await Page.enable();
  await DOM.enable();
  await Network.enable();
  await Network.setCacheDisabled({cacheDisabled: true});
  await Network.setUserAgentOverride({'userAgent': user_agent});

  await Page.navigate({url: target});

  Page.loadEventFired(async () => {

    // measure the height of the rendered page and use Emulation.setVisibleSize
    const {root: {nodeId: documentNodeId}} = await DOM.getDocument();

    const {nodeId: bodyNodeId} = await DOM.querySelector({
      selector: 'body',
      nodeId: documentNodeId,
    });

    const {model: {height}} = await DOM.getBoxModel({nodeId: bodyNodeId});

    const deviceMetrics = {
      width: viewportWidth,
      height: height,
      deviceScaleFactor: 1,
      mobile: false,
      fitWindow: false,
    };

    await Emulation.setDeviceMetricsOverride(deviceMetrics);
    await Emulation.setVisibleSize({width: viewportWidth, height: height});

    // get the base64 screenshot.
    const screenshot = await Page.captureScreenshot({format});

    // Save the base64 screenshot to binary image file
    //const buffer = new Buffer(screenshot.data, 'base64');

    console.log(screenshot.data);

    client.close();

  });

}).on('error', err => {
  console.error('Cannot connect to browser:', err);
});