const CDP = require('chrome-remote-interface');

const target = process.argv[2] + "";

const options = {
  host: process.argv[3] + "",
  port: process.argv[4] * 1
};

const user_agent = process.argv[5] + "";

CDP(options, (client) => {

  const {Network, Page} = client;

  Network.requestWillBeSent((params) => {
    console.log(
      JSON.stringify(params)
    );
  });

  Network.responseReceived((res) => {
    console.log(
      JSON.stringify(res)
    );
  });

  Page.loadEventFired(() => {
    client.close();
  });

  Promise.all([
    Network.enable(),
    Page.enable(),
    Network.setUserAgentOverride({'userAgent': user_agent}),
    Network.setCacheDisabled({ cacheDisabled : true })
  ]).then(() => {
    return Page.navigate({url: target});
  }).catch((err) => {
    console.error(`ERROR: ${err.message}`);
    client.close();
  });

}).on('error', (err) => {
  console.error('Cannot connect to remote endpoint:', err);
});
