const CDP = require('chrome-remote-interface');

const target = process.argv[2] + "";

const options = {
  host: process.argv[3] + "",
  port: process.argv[4] * 1
};

const user_agent = process.argv[5] + "";

CDP(options, async(client) => {

  const { Network, Page, Runtime } = client;

  try {
    await Network.enable();
    await Page.enable();
    await Network.setCacheDisabled({cacheDisabled: true});
    await Network.setUserAgentOverride({'userAgent': user_agent});
    await Page.navigate({url: target});
    await Page.loadEventFired();
    const result = await Runtime.evaluate({
      expression: 'document.documentElement.outerHTML'
    });
    const html = result.result.value;
    console.log(html);
  } catch (err) {
    console.error(err);
  } finally {
    client.close();
  }

}).on('error', (err) => {
    console.error(err);
});